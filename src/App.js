import './App.css';
import Home from './Home';
// import Publish from './Publish';
// import Hero from './Hero';
// import Footer from './Footer';
import Login from './Login';
import SignUp from './SignUp';
import Feed from './Feed';
import Settings from './Settings';
import {
  BrowserRouter as Router,
  Switch,
  Route
  // Link,
  // Redirect,
  // BrowserRouter
} from "react-router-dom";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";
require('firebase/auth');

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <PrivateRoute exact path="/feed" component={Feed} />
          <PrivateRoute exact path="/settings" component={Settings} />
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;
