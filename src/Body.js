import './Body.css'
import React from 'react';
import Story from './Story';
// import me from './images/me.jpeg';
// import { Timeline } from 'react-twitter-widgets'


class Body extends React.Component {

    render() {
        return (
            <div className="body">

                {/* <div className="intro">
                    <div className="intro_left">
                        <h2>Hi, I'm David. Share with me and others.</h2>

                    </div>
                    <div className="intro_right">

                    </div>
                </div> */}

                <div className="intro-2">
                    <p>Hi, Welcome to a space for sharing recommendations. Feel free to share with me; but most importantly, be kind to yourself and others</p>
                </div>
                {/* 
                <div className='story'>
                    <iframe title="apple music" allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" width="100%" height="450" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/fr/playlist/runtitled-ii/pl.u-V9D7g4pTRmyJVr?l=en"></iframe>
                </div>

                <div className='story'>
                    <iframe title="spotify" src="https://open.spotify.com/embed/playlist/0JKcPk6Kc0gxuTsSkyDAiM" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                </div> */}

                <Story title="Music" image="https://thehoya.com/wp-content/uploads/2019/10/19.10.30_Pony_rexorangecountyTwitter.png" link="https://open.spotify.com/track/2gpWyfu7eZ01zzncHpxOtA?si=e3479cbcd5c74034"/>
                <Story title="Film" image="https://cdn.vox-cdn.com/thumbor/Svw3wGya2dO56JFraQ6HM2Y0vMM=/1400x1050/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10225637/black_panther.jpg" link="https://www.rottentomatoes.com/tv/narcos"/>
                <Story title="Game" image="https://cdn.mos.cms.futurecdn.net/aHwkeLgSdV5Bhq696bjdKk.jpg" link="https://www.xbox.com/en-US/games/halo-infinite"/>

                {/* 
                <div className='starred_story'>
                    <Timeline
                        dataSource={{
                            sourceType: 'profile',
                            screenName: 'bibibibibambiba'
                        }}
                        options={{
                            theme: "white", width: "600",
                            height: '200'
                        }}
                    />

                </div> */}

            </div>

        )
    }
}

export default Body