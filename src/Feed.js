import React from 'react'
import './Feed.css'
// import { app } from "./base";
import Hero from './Hero';
import Footer from './Footer';
import Publish from './Publish';

function Feed() {
    return (
        <div>
            <Hero />
            <Publish />
            <Footer />
        </div>
    )
}

export default Feed
