import React from 'react';
import './Hero.css';
import { Link } from 'react-router-dom';
import { Avatar } from "@material-ui/core";
import PublicIcon from '@mui/icons-material/Public';
// import { Container } from 'react-bootstrap';
// import { Button } from "@material-ui/core";



class Hero extends React.Component {

    constructor() {
        super();
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var today = new Date(),
            date = months[(today.getMonth())] + ' ' + today.getDate() + ', ' + today.getFullYear();

        this.state = {
            date: date
        };
    }
    render() {
        return (

            <div className="hero">
                <div className="header">
                    <Link to="/">
                        <div className="header_left">
                            <img alt='bidicon' src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/SNice.svg/220px-SNice.svg.png" />
                        </div>
                    </Link>
                    <div className="header_right">
                        <div className="nav">
                            <Link to="/feed">
                                <ul>
                                    {/* <li><Link to="/publish" className="text-link">CULTURE</Link></li> */}
                                    <PublicIcon className="hero_icons" />
                                </ul>
                            </Link>
                        </div>
                        <div className="date">
                            <p>{this.state.date}</p>
                        </div>
                        <div className="settings">
                            <Link to="/settings">
                                <Avatar className="settings_icons" alt="pp" src="" />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>




        )


    }

}


export default Hero;