import './Home.css'
import Hero from './Hero';
import Footer from './Footer';
import Body from './Body';


function Home() {

    return (
        <div className="home">
            <Hero />
            <Body />
            <Footer />
        </div>
    );
}

export default Home;