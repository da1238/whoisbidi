import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import { app } from "./base";
import "./Login.css";
import { AuthContext } from "./Auth.js";
// import { Link } from 'react-router-dom';
// import firebase from "firebase";
// import { Button } from "@material-ui/core";

// email Login
const Login = ({ history }) => {
  const handleLogin = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div className='login'>

      <div className='login_desc'>

      </div>

      <div className='login_form'>
        <form onSubmit={handleLogin} className="form_login">
        <h1>PNFLGN</h1>
          <label>
            <input name="email" type="email" placeholder="Email" />
          </label>
          <label>
            <input name="password" type="password" placeholder="Password" />
          </label>
          <button type="submit">Log in</button>
        </form>

        <p>Don't have an account? <a className="sign_up_btn" href="/signup"> Sign Up </a></p>

      </div>


    </div>
  );
};

export default withRouter(Login);