import React from 'react'
import './PostDisplay.css'
// import { app } from './base';

export const PostDisplay = ({ post }) => {
    // const [name] = React.useState(post.name);
    const [content] = React.useState(post.content);
    // const [url] = React.useState(post.url);
    const [color] = React.useState(post.color);
    // console.log(color)

    return (
        <div className="postDisplay" style={{ backgroundColor: color }}>
            {/* <div className="postImage">
                <img alt='bidicon' src={url} />
            </div> */}
            <div className="postText">
                <p>{content}</p>
                <p>{color}</p>
            </div>


        </div>


    )
}

export default PostDisplay
