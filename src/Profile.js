import React, { useContext } from "react";
import './Profile.css';
import { AuthContext } from "./Auth.js";
import { app } from "./base";
require("firebase/auth");
require("firebase/database");

function Profile() {

    const { currentUser } = useContext(AuthContext);
    const userEmail = currentUser.email;
    var myUserId = currentUser.uid;
    console.log(myUserId)
    console.log(userEmail)

    return (

        <div className="profile">
            <div className="profile_body">
                <button className="logout" onClick={() => app.auth().signOut()}> Log Out</button>
            </div>
        </div>
    )
}

export default Profile
