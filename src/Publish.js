import React from "react";
import { app } from "./base";
import './Publish.css';
import Timeline from "./Timeline";
// import CheckCircleIcon from '@mui/icons-material/CheckCircle';

function Publish() {

  const [posts, setPost] = React.useState([]);
  // const [newPostName, setNewPostName] = React.useState();
  const [newPostContent, setNewPostContent] = React.useState();
  // const [newPostGenre, setNewPostGenre] = React.useState();
  // const [newUrl, setNewUrl] = React.useState();
  var randomColor = Math.floor(Math.random() * 16777215).toString(16);
  var colorString = "'#" + randomColor + "'";
  // const [newPostUploadDate] = React.useState(Date().toLocaleString());
  // const newPostDate = React.useState(new Date('2021-01-18T21:11:54'));

  // const onChange = (e) => {
  //   // UID

  //   const fileUploaded = e.target.files[0];
  //   console.log(fileUploaded.name);
  //   console.log(fileUploaded.size);
  //   console.log(fileUploaded.type);


  //   const pictureRef = app.storage().ref(`pictures/` + fileUploaded.name);
  //   pictureRef.put(fileUploaded).then(() => {
  //     // push picture data to firebase
  //     const storageRef = app.storage().ref(`/picture`);
  //     storageRef
  //       .child(fileUploaded.name)
  //       .getDownloadURL()
  //       .then((url) => {
  //         // setNewUrl(url)

  //       });
  //   });

  //   pictureRef.put(fileUploaded).then(() => {
  //     fileUploaded.value = "";
  //     console.log(fileUploaded.value);
  //     console.log("uploaded file successfully");
  //     alert("File uploaded successfully");

  //   });

  // };


  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("post").get();
      setPost(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);



  const onCreate = () => {
    if (newPostContent !== undefined) {

      const db = app.firestore();
      db.collection("post").add(
        {
          // name: newPostName,
          content: newPostContent,
          url: 'newUrl',
          color: colorString,
          genre: 'newPostGenre'
        })
        .then(() => {
          alert("Document successfully written!");
          console.log(posts);
          window.location.reload();
        })
        .catch((error) => {
          alert("Error writing document: ", error);
        });
    }
    else {
      alert('Image or Text Missing')
    }

  };




  return (
    <div className="p_body">

      <div className="publish">
        {/* <h6>Remember!</h6>
        <p>This is a safe space. Be kind to yourself and others</p> */}
        <div className="publish_header">
          <div className="publish_left">
            <img alt='comedy' src="https://pbs.twimg.com/media/C8PDDBMUwAAuid4.jpg" />
          </div>

        </div>
        <div className="publish_input">
          {/* <input
            value={newPostName}
            onChange={e => setNewPostName(e.target.value)}
            name="post_name"
            placeholder="input username"
          /> */}
          <textarea
            maxLength="60"
            value={newPostContent}
            onChange={e => setNewPostContent(e.target.value)} cols="40" rows="5"
            placeholder="enter your recommendation ..."
            name="post_desc"></textarea>

          <div className="publish_buttons">

            {/* Image upload button */}


            {/* <button onClick={onCreate}>Post</button> */}
            {/* <div className="upload" >
  <input type="file" onChange={onChange} />
</div> */}

            {/* <CheckCircleIcon className="createButton" onClick={onCreate} /> */}
            <button className="submit" onClick={onCreate}>Sbmt</button>

          </div>
        </div>

      </div>

      <Timeline />
    </div>

  )
}

export default Publish;