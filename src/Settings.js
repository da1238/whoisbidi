import './Home.css'
import Hero from './Hero';
import Footer from './Footer';
// import Body from './Body';
import Profile from './Profile';


function Settings() {

    return (
        <div className="home">
            <Hero />
            <Profile />
            <Footer />
        </div>
    );
}

export default Settings;