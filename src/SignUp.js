import React, { useCallback } from "react";
import { withRouter } from "react-router";
import { app } from "./base";
// import { Link } from 'react-router-dom'
// import Button from '@material-ui/core/Button';
import "./SignUp.css";


// random number generator
const crypto = require("crypto");
const rand = crypto.randomBytes(16).toString("hex");

const SignUp = ({ history }) => {

  const handleSignUp = useCallback(async event => {
    event.preventDefault();
    const { email, password } = event.target.elements;
    const userRef = app.database().ref(`users`);
    try {
      await app
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value);
      userRef.push({
        email: email.value,
        username: "user" + rand,
        profile_picture: "imageUrl"
      });

      history.push("/");
    } catch (error) {
      alert(error);
    }
  }, [history]);

  return (
    <div className='signup'>
{/* 
      <div className='signup_header'>
       
      </div> */}

      <form onSubmit={handleSignUp} className="form_signup">
      <h1>PNFSGP</h1>
        <label>
          <input name="email" type="email" placeholder="Email" />
        </label>
        <label>
          <input name="password" type="password" placeholder="Password" />
        </label>
        <button type="submit">Sign Up</button>
      </form>
      <p>Already have an account? <a className="login_btn" href="/login"> Login </a></p>
    </div>
  );
};

export default withRouter(SignUp);