import React from 'react'
import './Story.css'

function Story({title, image, link}) {
    return (
        <a href={link} target="_blank" rel="noreferrer" >
        <div className="story_body">
            <h1>{title}</h1>
            <img src={image} alt="background"/>
            
        </div>
        </a>
    )
}

export default Story
