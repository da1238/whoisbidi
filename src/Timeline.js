import React from 'react'
import { app } from "./base";
import { PostDisplay } from "./PostDisplay";
import './Timeline.css';

function Timeline() {
    const [posts, setPost] = React.useState([]);
    // const [content, setContent] = React.useState([]);

    React.useEffect(() => {
        const fetchData = async () => {
            const db = app.firestore();
            const data = await db.collection("post").get();
            setPost(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
            // setContent(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
        };
        fetchData();
    }, []);

    return (
        <div className='tl'>
            {posts.map(post => (
                    <div key={post.color}>
                        <PostDisplay post={post} />
                    </div>

                ))}
        </div>
    )
}

export default Timeline
